# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-ignorewarnings
##-dontoptimize
#-keep class * {
#    public private *;
#}
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

#AddPasswordActivity
#-keep public class * extends android.content.Intent
#-keep public class * extends android.os.Bundle
#-keep public class * extends android.support.annotation.Nullable
#-keep public class * extends android.support.v7.app.AppCompatActivity
#-keep public class * extends android.widget.Toast
#-keep public class * extends android.widget.EditText
#-keep public class * extends android.widget.Button
#-keep public class * extends android.view.View

-keep  class  android.content.Intent.**{ *;}
-keep  class  android.os.Bundle.**{ *;}
-keep  class  android.support.annotation.Nullable.**{ *;}
-keep  class  android.support.v7.app.AppCompatActivity.**{ *;}
-keep  class  android.widget.Toast.**{ *;}
-keep  class  android.widget.EditText.**{ *;}
-keep  class  android.widget.Button.**{ *;}
-keep  class  android.view.View.**{ *;}

#AddPatternActivity
#-keep public class * extends android.support.design.widget.CoordinatorLayout
#-keep public class * extends com.andrognito.patternlockview
#-keep public class * extends android.support.v7.widget.AppCompatTextView
-keep class android.support.design.widget.CoordinatorLayout.**{*;}
-keep class com.andrognito.patternlockview.**{*;}
-keep class android.support.v7.widget.AppCompatTextView.**{*;}

#CredentialsListAdapter
-keep  class   android.support.v7.widget.RecyclerView.**{*;}
-keep  class  android.view.LayoutInflater.**{*;}
-keep  class android.view.ViewGroup.**{*;}
-keep  class  android.widget.ImageView.**{*;}
-keep  class  android.widget.TextView.**{*;}

#DBUtil
-keep class  android.annotation.SuppressLint.**{*;}
-keep class  android.content.ContentValues.**{*;}
-keep class  android.content.Context.**{*;}
-keep class  android.database.**{*;}

#EncryptDecryptStringWithDES
-keep class  com.sun.mail.util.**{*;}
-keep class  javax.crypto.**{*;}

#DrawPatternActivity
-keep  class  android.support.design.widget.Snackbar.**{*;}
-keep  class  android.support.v7.widget.Toolbar.**{*;}
-keep  class  android.view.Menu.**{*;}
-keep  class  android.view.MenuItem.**{*;}
-keep  class  com.andrognito.patternlockview.**{*;}
-keep  class  java.util.List.**{*;}

#SessionManager
-keep  class android.content.SharedPreferences.**{*;}

#MainActivity
-keep class  android.content.DialogInterface.**{}
-keep class  android.support.design.widget.FloatingActionButton.**{}
-keep class  android.support.v7.app.AlertDialog.**{}
-keep class  android.support.v7.widget.LinearLayoutManager.**{}
-keep class  java.util.ArrayList.**{}

