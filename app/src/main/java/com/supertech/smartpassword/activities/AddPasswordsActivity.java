package com.supertech.smartpassword.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.supertech.smartpassword.R;
import com.supertech.smartpassword.utils.DBUtil;
import com.supertech.smartpassword.utils.EncryptionDecryptionUtil;

public class AddPasswordsActivity extends AppCompatActivity {
    private EditText edt_password, edt_user, edt_description;
    private boolean isUpdate;
    private int id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_passwords);
        edt_password = findViewById(R.id.edt_password);
        edt_user = findViewById(R.id.edt_user);
        edt_description = findViewById(R.id.edt_description);


        Intent intent = getIntent();
        if (intent.getStringExtra("Username") != null) {
            isUpdate = true;
            edt_user.setText(intent.getStringExtra("Username"));
            edt_password.setText(intent.getStringExtra("Password"));
            edt_description.setText(intent.getStringExtra("Description"));
            id = (intent.getIntExtra("RowId", -1));
        }

        findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = edt_user.getText().toString();
                String pswd = edt_password.getText().toString();
                String description = edt_description.getText().toString();
                if (username.trim().isEmpty()) {
                    Toast.makeText(AddPasswordsActivity.this, "Username cannot be left blank", Toast.LENGTH_SHORT).show();
                } else if (pswd.trim().isEmpty()) {
                    Toast.makeText(AddPasswordsActivity.this, "Password cannot be left blank", Toast.LENGTH_SHORT).show();
                } else if (description.trim().isEmpty()) {
                    Toast.makeText(AddPasswordsActivity.this, "Description cannot be left blank", Toast.LENGTH_SHORT).show();
                } else {
                    EncryptionDecryptionUtil des =EncryptionDecryptionUtil.getInstance(AddPasswordsActivity.this);
                    byte[] name = des.encrypt(username);
                   byte[] pass = des.encrypt(pswd);
                    if (isUpdate) {
                        DBUtil.getInstance(AddPasswordsActivity.this).update(id, name, pass, description);

                    } else
                        DBUtil.getInstance(AddPasswordsActivity.this).insert(name, pass, description);

                    setResult(101);
                    finish();


                }

            }
        });
    }
}
