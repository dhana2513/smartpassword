package com.supertech.smartpassword.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.supertech.smartpassword.custom.views.patternlockview.PatternLockView;
import com.supertech.smartpassword.R;
import com.supertech.smartpassword.custom.views.patternlockview.listener.PatternLockViewListener;
import com.supertech.smartpassword.custom.views.patternlockview.utils.PatternLockUtils;
import com.supertech.smartpassword.utils.DBUtil;
import com.supertech.smartpassword.utils.EncryptionDecryptionUtil;
import com.supertech.smartpassword.utils.SessionManagerUtil;

import java.util.List;

public class AddPatternActivity extends AppCompatActivity {
    private AppCompatTextView txt_pattern_text, txt_pattern_msg;
    private PatternLockView mPatternLockView;
    private String FirstPIN = "";
    private String FirstConfirm = "";
    private int occurance = 0;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_set_password);
        txt_pattern_text = findViewById(R.id.txt_pattern_text);
        txt_pattern_msg = findViewById(R.id.txt_pattern_msg);
        mPatternLockView = findViewById(R.id.pattern_lock_view);
        mPatternLockView.addPatternLockListener(mPatternLockViewListener);

        if (SessionManagerUtil.getInstance(this).getMasterpassword() == null) {
            final RelativeLayout rl_pattern;
            LinearLayout rl_masterPassword;
            rl_pattern = findViewById(R.id.rl_pattern);
            rl_masterPassword = findViewById(R.id.rl_masterPassword);

            rl_pattern.setVisibility(View.GONE);
            rl_masterPassword.setVisibility(View.VISIBLE);

            final EditText edtMasterPassword = findViewById(R.id.edtMasterPassword);
            final EditText edtConfirmMasterPassword = findViewById(R.id.edtConfirmMasterPassword);
            Button btnMasterPassword = findViewById(R.id.btnSet);


            btnMasterPassword.setOnClickListener(view -> {

                String passwd = edtMasterPassword.getText().toString();
                String confirmPasswd = edtConfirmMasterPassword.getText().toString();
                if (passwd.length() < 8) {
                    edtMasterPassword.setError("Password is too short enter minimum 8 character");
                    return;
                }
                if (!passwd.equals(confirmPasswd)) {
                    edtConfirmMasterPassword.setError("Password not match");
                } else {

                    byte[] pass = EncryptionDecryptionUtil.encryptMasterPassword(passwd);
                    SessionManagerUtil.getInstance(AddPatternActivity.this).setMasterpassword(pass);
                    rl_pattern.setVisibility(View.VISIBLE);
                    rl_masterPassword.setVisibility(View.GONE);
                }
            });
        }

        if (!SessionManagerUtil.getInstance(this).getIsLocked()) {
            if (!SessionManagerUtil.getInstance(this).getIsPatternSaved()) {
                txt_pattern_text.setText("Draw New Pattern");
            } else {
                txt_pattern_text.setText("To Change Pattern, Draw old Pattern");
            }

        } else {
            txt_pattern_text.setText("Pattern is locked");
            txt_pattern_msg.setText("Pattern lock! more than 3 invalid attempts occured! try to unlock with master password");
            mPatternLockView.removePatternLockListener(mPatternLockViewListener);
        }
    }

    private PatternLockViewListener mPatternLockViewListener = new PatternLockViewListener() {
        @Override
        public void onStarted() {
            Log.d(getClass().getName(), "Pattern drawing started");
        }

        @Override
        public void onProgress(List<PatternLockView.Dot> progressPattern) {
            Log.d(getClass().getName(), "Pattern progress: " +
                    PatternLockUtils.patternToString(mPatternLockView, progressPattern));
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onComplete(List<PatternLockView.Dot> pattern) {
            Log.d(getClass().getName(), "Pattern complete: " + PatternLockUtils.patternToString(mPatternLockView, pattern));
            txt_pattern_msg.setText("");
            if (occurance > 3) {
                txt_pattern_msg.setText("Pattern lock! more than 3 invalid attempts occured!");
                SessionManagerUtil.getInstance(AddPatternActivity.this).setLock(true);
                txt_pattern_text.setText("Pattern is locked");
                mPatternLockView.removePatternLockListener(mPatternLockViewListener);
                mPatternLockView.clearPattern();
                return;
            }
            String secondPIN;
            if (!SessionManagerUtil.getInstance(AddPatternActivity.this).getIsPatternSaved()) {
                if (PatternLockUtils.patternToString(mPatternLockView, pattern).length() > 3) {
                    if (FirstPIN.isEmpty()) {
                        FirstPIN = PatternLockUtils.patternToString(mPatternLockView, pattern);
                        txt_pattern_text.setText("Confirm Pattern Again!");
                        mPatternLockView.clearPattern();
                    } else {
                        secondPIN = PatternLockUtils.patternToString(mPatternLockView, pattern);
                        if (FirstPIN.equalsIgnoreCase(secondPIN)) {
                            Toast.makeText(AddPatternActivity.this, "Pattern Saved!", Toast.LENGTH_SHORT).show();
                            byte[] DES = EncryptionDecryptionUtil.getInstance(AddPatternActivity.this).encrypt(FirstPIN);
                            DBUtil.getInstance(AddPatternActivity.this).putPattern(DES);
                            SessionManagerUtil.getInstance(AddPatternActivity.this).setIsPattenSaved(true);
                            finish();
                        } else {
                            txt_pattern_msg.setText("Pattern not matched ,try again!");
                            FirstPIN = "";
                            txt_pattern_text.setText("Draw New Pattern");
                            mPatternLockView.clearPattern();
                        }
                    }
                } else {
                    txt_pattern_msg.setText("Pattern must be join of more than 4 dots ,try again!");
                    FirstPIN = "";
                    txt_pattern_text.setText("Draw New Pattern");
                    mPatternLockView.clearPattern();
                }
            } else {

                if (FirstConfirm.isEmpty()) {
                    byte[] DESDecp = DBUtil.getInstance(AddPatternActivity.this).getPattern();
                    String DES =EncryptionDecryptionUtil.getInstance(AddPatternActivity.this).decrypt(DESDecp);
                    FirstConfirm = PatternLockUtils.patternToString(mPatternLockView, pattern);
                    if (DES.equalsIgnoreCase(FirstConfirm)) {
                        txt_pattern_text.setText("Draw New Pattern");
                        mPatternLockView.clearPattern();
                        return;
                    } else {
                        txt_pattern_msg.setText("Pattern not matched ,try again!");
                        mPatternLockView.clearPattern();
                        occurance++;
                        FirstConfirm = "";
                        return;
                    }
                }
                if (PatternLockUtils.patternToString(mPatternLockView, pattern).length() > 3) {
                    if (FirstPIN.isEmpty()) {
                        FirstPIN = PatternLockUtils.patternToString(mPatternLockView, pattern);
                        txt_pattern_text.setText("Confirm Pattern Again!");
                        mPatternLockView.clearPattern();
                    } else {
                        secondPIN = PatternLockUtils.patternToString(mPatternLockView, pattern);
                        if (FirstPIN.equalsIgnoreCase(secondPIN)) {
                            byte[] DES = EncryptionDecryptionUtil.getInstance(AddPatternActivity.this).encrypt(FirstPIN);
                            DBUtil.getInstance(AddPatternActivity.this).putPattern(DES);
                            SessionManagerUtil.getInstance(AddPatternActivity.this).setIsPattenSaved(true);
                            Toast.makeText(AddPatternActivity.this, "Pattern Saved!", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            txt_pattern_msg.setText("Pattern not matched ,try again!");
                            FirstPIN = "";
                            txt_pattern_text.setText("Draw New Pattern");
                            mPatternLockView.clearPattern();
                        }
                    }
                } else {

                    txt_pattern_msg.setText("Pattern must be join of more than 4 dots ,try again!");
                    FirstPIN = "";
                    txt_pattern_text.setText("Draw New Pattern");
                    mPatternLockView.clearPattern();
                }
            }
        }

        @Override
        public void onCleared() {
            Log.d(getClass().getName(), "Pattern has been cleared");
        }
    };
}

