package com.supertech.smartpassword.activities.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.supertech.smartpassword.R;
import com.supertech.smartpassword.activities.main.adapter.ViewPagerAdapter;
import com.supertech.smartpassword.activities.main.fragments.CredentialListFragment;
import com.supertech.smartpassword.activities.main.fragments.ImageListFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Menu mMenu;
    private MenuItem mLastSelectedMenuItem;
    private ImageListFragment mImageListFragment;
    private CredentialListFragment mCredentialListFragment;
    public SearchView mSearchView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_pwd_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();
    }

    private void init() {
        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        ArrayList<Fragment> listFragment = new ArrayList<>();
        mCredentialListFragment = new CredentialListFragment();
        mImageListFragment = new ImageListFragment();
        listFragment.add(mCredentialListFragment);
        listFragment.add(mImageListFragment);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), listFragment);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mLastSelectedMenuItem.setVisible(false);
                mLastSelectedMenuItem = mMenu.getItem(position);
                mLastSelectedMenuItem.setVisible(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_xml, menu);
        mMenu = menu;
        MenuItem search_item = menu.findItem(R.id.mi_search);
        menu.getItem(1).setOnMenuItemClickListener(menuItem -> {
            mImageListFragment.OnAddImageButtonClick();
            return false;
        });
        mLastSelectedMenuItem = search_item;
        mSearchView = (SearchView) search_item.getActionView();
        mSearchView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mSearchView.setFocusable(false);
        mSearchView.setQueryHint("Search");
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                mCredentialListFragment.onSearch(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                mCredentialListFragment.onSearch(s);
                return false;
            }
        });
        mSearchView.setOnCloseListener(() -> {
            mCredentialListFragment.onSearchClose();
            return false;
        });

        return true;
    }

}
