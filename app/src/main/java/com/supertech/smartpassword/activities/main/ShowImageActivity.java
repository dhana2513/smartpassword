package com.supertech.smartpassword.activities.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.supertech.smartpassword.R;
import com.supertech.smartpassword.activities.main.adapter.UserImage;
import com.supertech.smartpassword.custom.views.ZoomableImageView;
import com.supertech.smartpassword.utils.EncryptionDecryptionUtil;

import java.util.Objects;

public class ShowImageActivity extends AppCompatActivity {
    public static String KEY_USER_IMAGE = "KEY_USER_IMAGE";
    private UserImage mUserImage;
    private ZoomableImageView imageView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActionBar().hide();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_show_image);
        imageView = findViewById(R.id.imageView);
        imageView.setRestrictBounds(false);
        mUserImage = getIntent().getParcelableExtra(KEY_USER_IMAGE);
//        imageView.setImageBitmap(Objects.requireNonNull(mUserImage).getImage());
        new LoadImageAsync(imageView, this).execute(mUserImage.getPath());

    }


    private static class LoadImageAsync extends AsyncTask<String, Void, Bitmap> {
        @SuppressLint("StaticFieldLeak")
        private ZoomableImageView imageView;
        @SuppressLint("StaticFieldLeak")
        private Context mContext;

        LoadImageAsync(ZoomableImageView imageView, Context mContext) {
            this.imageView = imageView;
            this.mContext = mContext;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            EncryptionDecryptionUtil encryptionDecryptionUtil = EncryptionDecryptionUtil.getInstance(mContext);
            return encryptionDecryptionUtil.decryptBitmap(strings[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
//            imageView.setImageBitmap(bitmap);
        }
    }

}
