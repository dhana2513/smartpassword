package com.supertech.smartpassword.activities.main.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.supertech.smartpassword.R;

public class CredentialsListAdapter extends ListAdapter<UserData, CredentialsListAdapter.MyViewHolder> {

    private OnItemSelect onItemSelect;
    private static final DiffUtil.ItemCallback<UserData> DIFF_CALLBACK = new DiffUtil.ItemCallback<UserData>() {
        @Override
        public boolean areItemsTheSame(@NonNull UserData oldItem, @NonNull UserData newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull UserData oldItem, @NonNull UserData newItem) {
            return oldItem.getId() == newItem.getId() &&
                    oldItem.getDescription().equals(newItem.getDescription()) &&
                    oldItem.getPassword().equals(newItem.getPassword()) &&
                    oldItem.getUsername().equals(newItem.getUsername());
        }
    };

    public CredentialsListAdapter() {
        super(DIFF_CALLBACK);
    }


//    public CredentialsListAdapter(ArrayList<UserData> mListUserData) {
//        this.mListUserData = mListUserData;
//    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_credentials_list, parent, false);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0,1,0,1);
        itemView.setLayoutParams(params);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.textView_username.setText(getItem(position).getUsername());
        holder.textView_password.setText(getItem(position).getPassword());
        holder.textView_header.setText(getItem(position).getDescription());
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView_username, textView_password, textView_header;
        ImageView iv_edit, iv_delete;

        MyViewHolder(View itemView) {
            super(itemView);

            textView_username = itemView.findViewById(R.id.textView_username);
            textView_password = itemView.findViewById(R.id.textView_password);
            textView_header = itemView.findViewById(R.id.txt_header);
            iv_edit = itemView.findViewById(R.id.img_edit);
            iv_delete = itemView.findViewById(R.id.img_delete);

            iv_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemSelect.editUserData(getItem(getAdapterPosition()));
                }
            });

            iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemSelect.deleteUserData(getAdapterPosition());
                }
            });

        }
    }

    public void setOnItemSelectListener(OnItemSelect onItemSelect) {
        this.onItemSelect = onItemSelect;
    }

    public interface OnItemSelect {
        void editUserData(UserData userData);

        void deleteUserData(int position);
    }
}
