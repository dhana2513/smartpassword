package com.supertech.smartpassword.activities.main.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.supertech.smartpassword.R;
import com.supertech.smartpassword.utils.DBUtil;
import com.supertech.smartpassword.utils.EncryptionDecryptionUtil;

public class ImageListAdapter extends ListAdapter<UserImage, ImageListAdapter.ViewHolder> {
    private OnItemSelect mOnItemSelect;
    private static final DiffUtil.ItemCallback<UserImage> DIFF_CALLBACK = new DiffUtil.ItemCallback<UserImage>() {
        @Override
        public boolean areItemsTheSame(@NonNull UserImage oldItem, @NonNull UserImage newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull UserImage oldItem, @NonNull UserImage newItem) {
            return oldItem.getId() == newItem.getId() && oldItem.getPath().equals(newItem.getPath());
        }
    };

    public ImageListAdapter() {
        super(DIFF_CALLBACK);
    }

//    public ImageListAdapter(ArrayList<UserImage> mListImages) {
//        this.mListUserImages = mListImages;
//    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_photo_list, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, viewGroup.getHeight() / 3);
        params.setMargins(5, 5, 5, 5);
        view.setLayoutParams(params);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        new LoadImageAsync(viewHolder.imageView, viewHolder.itemView.getContext()).
                execute(getItem(i).getPath());
//        viewHolder.imageView.setImageBitmap(mListUserImages.get(i).getImage());
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView imageView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(view -> mOnItemSelect.onItemClick(getItem(getAdapterPosition())));
            itemView.setOnCreateContextMenuListener((menu, v, menuInfo) -> {
                MenuItem item = menu.add(0, v.getId(), 0, "Delete");
                item.setOnMenuItemClickListener(item1 -> {
                    DBUtil util = DBUtil.getInstance(itemView.getContext());
                    util.deleteImage(getItem(getAdapterPosition()));
                    return false;
                });
            });
        }
    }

    public void setOnItemSelectListener(OnItemSelect onItemSelect) {
        this.mOnItemSelect = onItemSelect;
    }

    public interface OnItemSelect {
        void onItemClick(UserImage userImage);

    }

    private static class LoadImageAsync extends AsyncTask<String, Void, Bitmap> {
        @SuppressLint("StaticFieldLeak")
        private AppCompatImageView imageView;
        @SuppressLint("StaticFieldLeak")
        private Context mContext;

        LoadImageAsync(AppCompatImageView imageView, Context mContext) {
            this.imageView = imageView;
            this.mContext = mContext;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            EncryptionDecryptionUtil encryptionDecryptionUtil = EncryptionDecryptionUtil.getInstance(mContext);
            return encryptionDecryptionUtil.decryptBitmap(strings[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
        }
    }

}
