package com.supertech.smartpassword.activities.main.adapter;

public class UserData {
    private int id;
    private String Username;
    private String password;
    private String description;

    public UserData(int id, String username, String password, String description) {
        this.id = id;
        Username = username;
        this.password = password;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
