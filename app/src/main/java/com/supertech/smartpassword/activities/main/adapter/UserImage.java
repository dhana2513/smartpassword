package com.supertech.smartpassword.activities.main.adapter;

import android.os.Parcel;
import android.os.Parcelable;


public class UserImage implements Parcelable {
    private int id;
    private String path;


    public UserImage(int id,String path) {
        this.id = id;
        this.path = path;
    }

    protected UserImage(Parcel in) {
        id = in.readInt();
        path = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(path);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserImage> CREATOR = new Creator<UserImage>() {
        @Override
        public UserImage createFromParcel(Parcel in) {
            return new UserImage(in);
        }

        @Override
        public UserImage[] newArray(int size) {
            return new UserImage[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getPath() {
        return path;
    }
}
