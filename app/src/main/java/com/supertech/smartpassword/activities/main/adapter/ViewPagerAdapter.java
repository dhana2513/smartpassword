package com.supertech.smartpassword.activities.main.adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> mListFragments;

    public ViewPagerAdapter(FragmentManager fm, ArrayList<Fragment> listFragments) {
        super(fm);
        this.mListFragments = listFragments;
    }

    @Override
    public Fragment getItem(int i) {
        return mListFragments.get(i);
    }

    @Override
    public int getCount() {
        return mListFragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return position == 0 ? "Credentials" : "Images";
    }
}
