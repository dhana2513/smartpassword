package com.supertech.smartpassword.activities.main.draw.pattern;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.Snackbar;
import com.supertech.smartpassword.R;
import com.supertech.smartpassword.activities.AddPatternActivity;
import com.supertech.smartpassword.activities.main.MainActivity;
import com.supertech.smartpassword.custom.views.patternlockview.PatternLockView;
import com.supertech.smartpassword.custom.views.patternlockview.listener.PatternLockViewListener;
import com.supertech.smartpassword.custom.views.patternlockview.utils.PatternLockUtils;
import com.supertech.smartpassword.fragments.UnlockFragment;
import com.supertech.smartpassword.utils.DBUtil;
import com.supertech.smartpassword.utils.EncryptionDecryptionUtil;
import com.supertech.smartpassword.utils.SessionManagerUtil;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class DrawPatternActivity extends AppCompatActivity implements UnlockFragment.OnUnlockWithMasterPassword {
    private PatternLockView mPatternLockView;
    private AppCompatTextView txt_pattern_msg, txt_pattern_text;

    private int count = 0;
    private CoordinatorLayout coordinatorLayout;

    private KeyStore keyStore;
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;
    private FingerprintHandler mFingerprintHandler;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        coordinatorLayout = findViewById(R.id.coordinatorlayout);
        setSupportActionBar(toolbar);

        mPatternLockView = findViewById(R.id.pattern_lock_view);
        mPatternLockView.addPatternLockListener(mPatternLockViewListener);
        txt_pattern_text = findViewById(R.id.txt_pattern_text);
        txt_pattern_msg = findViewById(R.id.txt_pattern_msg);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SessionManagerUtil.getInstance(this).getIsLocked()) {
            txt_pattern_text.setText("Pattern is locked");
            txt_pattern_msg.setText("Pattern lock! more than 3 invalid attempts occured! try to unlock with master password");
            mPatternLockView.removePatternLockListener(mPatternLockViewListener);
        } else if (!SessionManagerUtil.getInstance(this).getIsPatternSaved()) {
            Snackbar.make(coordinatorLayout, "Create Pattern First!", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
            startActivity(new Intent(DrawPatternActivity.this, AddPatternActivity.class));
        } else {
            txt_pattern_text.setText("Draw pattern");
            txt_pattern_msg.setText("");
            initFingerprintSenssor();
        }
    }

    private void initFingerprintSenssor() {
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (fingerprintManager != null && fingerprintManager.isHardwareDetected()) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) == PackageManager.PERMISSION_GRANTED) {
                    if (!fingerprintManager.hasEnrolledFingerprints()) {
                        txt_pattern_msg.setText("Register at least one fingerprint in Settings");
                    } else {
                        assert keyguardManager != null;
                        if (!keyguardManager.isKeyguardSecure()) {
                            txt_pattern_msg.setText("Lock screen security not enabled in Settings");
                        } else {
                            generateKey();
                            if (cipherInit()) {
                                FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                mFingerprintHandler = new FingerprintHandler(this, this);
                                mFingerprintHandler.startAuth(fingerprintManager, cryptoObject);
                            }
                        }
                    }
                }
            }
        }
    }


    private PatternLockViewListener mPatternLockViewListener = new PatternLockViewListener() {
        @Override
        public void onStarted() {
            Log.d(getClass().getName(), "Pattern drawing started");
        }

        @Override
        public void onProgress(List<PatternLockView.Dot> progressPattern) {
            Log.d(getClass().getName(), "Pattern progress: " +
                    PatternLockUtils.patternToString(mPatternLockView, progressPattern));
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onComplete(List<PatternLockView.Dot> pattern) {
            Log.d(getClass().getName(), "Pattern complete: " +
                    PatternLockUtils.patternToString(mPatternLockView, pattern));
            if (SessionManagerUtil.getInstance(DrawPatternActivity.this).getIsPatternSaved()) {
                if (count < 4) {
                    byte[] DESDecp = DBUtil.getInstance(DrawPatternActivity.this).getPattern();
                    String DES = EncryptionDecryptionUtil.getInstance(DrawPatternActivity.this).decrypt(DESDecp);
                    String FirstConfirm = PatternLockUtils.patternToString(mPatternLockView, pattern);
                    if (DES.equalsIgnoreCase(FirstConfirm)) {
                        onAuthenticationSuccess();
                    } else {
                        txt_pattern_msg.setText("Pattern not matched ,try again!");
                        mPatternLockView.clearPattern();
                        count++;
                    }
                } else {
                    txt_pattern_msg.setText("Pattern lock! more than 3 invalid attempts occured! try to unlock with master password");
                    SessionManagerUtil.getInstance(DrawPatternActivity.this).setLock(true);
                    invalidateOptionsMenu();
                    txt_pattern_text.setText("Pattern is locked");
                    mPatternLockView.removePatternLockListener(mPatternLockViewListener);
                    mPatternLockView.clearPattern();
                }
            } else {
                mPatternLockView.clearPattern();

                Snackbar.make(coordinatorLayout, "Create Pattern First!", Snackbar.LENGTH_SHORT).show();
                startActivity(new Intent(DrawPatternActivity.this, AddPatternActivity.class));
            }
        }

        @Override
        public void onCleared() {
            Log.d(getClass().getName(), "Pattern has been cleared");
        }
    };

    public void onAuthenticationSuccess() {
        cancelFingerPrintListener();
        mPatternLockView.clearPattern();
        Intent intent = new Intent(DrawPatternActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void cancelFingerPrintListener() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (mFingerprintHandler != null) {
                    mFingerprintHandler.cancel();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if (SessionManagerUtil.getInstance(this).getIsLocked()) {
            MenuItem menuItem = menu.findItem(R.id.action_changePattern);
            menuItem.setVisible(false);
        } else {
            MenuItem menuItem = menu.findItem(R.id.action_unlockWithMasterPassword);
            menuItem.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_changePattern) {
            cancelFingerPrintListener();
            startActivity(new Intent(DrawPatternActivity.this, AddPatternActivity.class));
            return true;
        }
        if (id == R.id.action_unlockWithMasterPassword) {
            cancelFingerPrintListener();
            UnlockFragment unlockFragment = new UnlockFragment();
            unlockFragment.setOnUnlockWithMasterPasswordListener(this);
            getSupportFragmentManager().beginTransaction().addToBackStack(null)
                    .add(R.id.frame, unlockFragment).commit();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void invalidate() {
        count = 0;
        txt_pattern_text.setText("Draw pattern");
        txt_pattern_msg.setText("");
        mPatternLockView.clearPattern();
        startActivity(new Intent(DrawPatternActivity.this, AddPatternActivity.class));
        mPatternLockView.addPatternLockListener(mPatternLockViewListener);
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }
}

