package com.supertech.smartpassword.activities.main.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.supertech.smartpassword.R;
import com.supertech.smartpassword.activities.AddPasswordsActivity;
import com.supertech.smartpassword.activities.main.MainActivity;
import com.supertech.smartpassword.activities.main.adapter.CredentialsListAdapter;
import com.supertech.smartpassword.activities.main.adapter.UserData;
import com.supertech.smartpassword.utils.DBUtil;

import java.util.ArrayList;
import java.util.Objects;

public class CredentialListFragment extends Fragment {
    private RecyclerView recyclerView;
    private CredentialsListAdapter mAdapter;
    private ArrayList<UserData> mListUserData;
    private TextView txt_no_credentials;

    public CredentialListFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lay_credentials_list, null);
        recyclerView = view.findViewById(R.id.recyclerView);
        txt_no_credentials = view.findViewById(R.id.txt_no_credentials);

        FloatingActionButton fab = view.findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fab.setEnabled(false);
                new Handler().postDelayed(() -> fab.setEnabled(true), 1000);
                startActivityForResult(new Intent(getContext(), AddPasswordsActivity.class), 100);
            }
        });


        mListUserData = DBUtil.getInstance(getContext()).getAllDatabaseData();
        mAdapter = new CredentialsListAdapter();
        mAdapter.submitList(mListUserData);
        mAdapter.setOnItemSelectListener(new OnUserDataSelect());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

        if (mListUserData.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            txt_no_credentials.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            txt_no_credentials.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {

            if (resultCode == 101) {
                mListUserData = DBUtil.getInstance(getContext()).getAllDatabaseData();
                mAdapter.submitList(mListUserData);
                if (mListUserData.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    txt_no_credentials.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    txt_no_credentials.setVisibility(View.GONE);
                }
            }
        }
    }

    public void onSearch(String text) {
        ArrayList<UserData> userDataArrayList = new ArrayList<>();
        for (UserData userData : mListUserData) {
            if (userData.getDescription().toLowerCase().contains(text.trim().toLowerCase())) {
                userDataArrayList.add(userData);
            }
        }
        mAdapter.submitList(userDataArrayList);
    }

    public void onSearchClose() {
        mAdapter.submitList(mListUserData);
    }

    private class OnUserDataSelect implements CredentialsListAdapter.OnItemSelect {
        @Override
        public void editUserData(UserData userData) {
            Intent intent = new Intent(getContext(), AddPasswordsActivity.class);
            intent.putExtra("Username", userData.getUsername());
            intent.putExtra("Password", userData.getPassword());
            intent.putExtra("Description", userData.getDescription());
            intent.putExtra("RowId", userData.getId());
            try {
                SearchView searchView = ((MainActivity) Objects.requireNonNull(getActivity())).mSearchView;
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            startActivityForResult(intent, 100);
        }

        @Override
        public void deleteUserData(final int position) {

            new AlertDialog.Builder(Objects.requireNonNull(getContext())).setTitle("Are you sure you want to delete ?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            DBUtil.getInstance(getContext()).delete(mListUserData.get(position).getId());
                            mListUserData = DBUtil.getInstance(getContext()).getAllDatabaseData();
                            mAdapter.submitList(mListUserData);
                            if (mListUserData.isEmpty()) {
                                recyclerView.setVisibility(View.GONE);
                                txt_no_credentials.setVisibility(View.VISIBLE);
                            } else {
                                recyclerView.setVisibility(View.VISIBLE);
                                txt_no_credentials.setVisibility(View.GONE);
                            }


                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            }).create().show();

        }
    }


}
