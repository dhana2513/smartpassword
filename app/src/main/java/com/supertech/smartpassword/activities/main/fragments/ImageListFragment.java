package com.supertech.smartpassword.activities.main.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.supertech.smartpassword.R;
import com.supertech.smartpassword.activities.main.CameraXActivity;
import com.supertech.smartpassword.activities.main.ShowImageActivity;
import com.supertech.smartpassword.activities.main.adapter.ImageListAdapter;
import com.supertech.smartpassword.activities.main.adapter.UserImage;
import com.supertech.smartpassword.utils.DBUtil;
import com.supertech.smartpassword.utils.EncryptionDecryptionUtil;

import java.util.ArrayList;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;
import static com.supertech.smartpassword.activities.main.ShowImageActivity.KEY_USER_IMAGE;

public class ImageListFragment extends Fragment {

    private ImageListAdapter mAdapter;
    private ArrayList<UserImage> mListUserImages;
    private RecyclerView recyclerView;
    private TextView txt_no_credentials;
    private static int RESULT_LOAD_IMAGE = 1354;

    private final int REQUEST_CODE_PERMISSIONS = 101;
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};

    public ImageListFragment() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getContext())).registerReceiver(mImageDatabaseChangeReceiver,
                new IntentFilter("com.supertech.NewImageAdded"));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (allPermissionsGranted()) {
            ActivityCompat.requestPermissions(getActivity(), REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lay_credentials_list, null);
        recyclerView = view.findViewById(R.id.recyclerView);
        txt_no_credentials = view.findViewById(R.id.txt_no_credentials);
        txt_no_credentials.setText("Added images will be shown here");
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(view1 -> {
            fab.setEnabled(false);
            new Handler().postDelayed(() -> fab.setEnabled(true), 1000);
            Objects.requireNonNull(getActivity()).startActivity(new Intent(getContext(), CameraXActivity.class));
        });
        mListUserImages = DBUtil.getInstance(getContext()).getAllImages();
        mAdapter = new ImageListAdapter();
        mAdapter.setOnItemSelectListener(userImage -> {
            Intent intent = new Intent(getContext(), ShowImageActivity.class);
            intent.putExtra(KEY_USER_IMAGE, userImage);
            startActivity(intent);
        });
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(mAdapter);
        refreshList();

        return view;
    }


    private void refreshList() {
        mListUserImages = DBUtil.getInstance(getContext()).getAllImages();
        if (mListUserImages.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            txt_no_credentials.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            txt_no_credentials.setVisibility(View.GONE);
        }
        mAdapter.submitList(mListUserImages);
    }

    private BroadcastReceiver mImageDatabaseChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshList();
        }
    };

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getContext())).unregisterReceiver(mImageDatabaseChangeReceiver);
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            try {
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                if(bitmap != null) {
                    EncryptionDecryptionUtil.getInstance(getContext()).saveImage(bitmap, picturePath);
                    try{

                    }catch (Exception e){}
                }else {
                    Toast.makeText(getContext(), "Oops an error has occurred", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void OnAddImageButtonClick() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                Toast.makeText(getContext(), "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean allPermissionsGranted() {

        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
        }
        return false;
    }
}
