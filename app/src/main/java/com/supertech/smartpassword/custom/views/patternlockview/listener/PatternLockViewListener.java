package com.supertech.smartpassword.custom.views.patternlockview.listener;

import com.supertech.smartpassword.custom.views.patternlockview.PatternLockView;

import java.util.List;

public interface PatternLockViewListener {

    void onStarted();

    void onProgress(List<PatternLockView.Dot> progressPattern);

    void onComplete(List<PatternLockView.Dot> pattern);

    void onCleared();
}