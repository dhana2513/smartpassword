package com.supertech.smartpassword.custom.views.patternlockview.utils;


import com.supertech.smartpassword.custom.views.patternlockview.PatternLockView;

import java.util.ArrayList;
import java.util.List;

public class PatternLockUtils {

    private PatternLockUtils() {
        throw new AssertionError("You can not instantiate this class. Use its static utility " +
                "methods instead");
    }

    public static String patternToString(PatternLockView patternLockView,
                                         List<PatternLockView.Dot> pattern) {
        if (pattern == null) {
            return "";
        }
        int patternSize = pattern.size();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < patternSize; i++) {
            PatternLockView.Dot dot = pattern.get(i);
            stringBuilder.append((dot.getRow() * patternLockView.getDotCount() + dot.getColumn()));
        }
        return stringBuilder.toString();
    }

    public static List<PatternLockView.Dot> stringToPattern(PatternLockView patternLockView,
                                                            String string) {
        List<PatternLockView.Dot> result = new ArrayList<>();

        for (int i = 0; i < string.length(); i++) {
            int number = Character.getNumericValue(string.charAt(i));
            result.add(PatternLockView.Dot.of(number / patternLockView.getDotCount(),
                    number % patternLockView.getDotCount()));
        }
        return result;
    }

}
