package com.supertech.smartpassword.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.supertech.smartpassword.R;
import com.supertech.smartpassword.utils.DBUtil;
import com.supertech.smartpassword.utils.EncryptionDecryptionUtil;
import com.supertech.smartpassword.utils.SessionManagerUtil;


public class UnlockFragment extends Fragment {
    private OnUnlockWithMasterPassword mOnUnlockWithMasterPassword;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setHasOptionsMenu(false);
        View view = inflater.inflate(R.layout.lay_frag_unlock_with_master_password, null);
        Button btnUnlock = view.findViewById(R.id.btnUnlock);
        final EditText edtMasterPassword = view.findViewById(R.id.edtMasterPassword);

        btnUnlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                byte[] pass = SessionManagerUtil.getInstance(getContext()).getMasterpassword();
                String passwd = EncryptionDecryptionUtil.decryptMasterPassword(pass);

                String text = edtMasterPassword.getText().toString();

                assert passwd != null;
                if (passwd.equals(text)) {

                    SessionManagerUtil.getInstance(getContext()).setIsPattenSaved(false);
                    SessionManagerUtil.getInstance(getContext()).setLock(false);
                    DBUtil.getInstance(getContext()).deletePattern();

                    mOnUnlockWithMasterPassword.invalidate();
                    assert getFragmentManager() != null;
                    getFragmentManager().popBackStack();

                } else {
                    edtMasterPassword.setError(" Incorrect Master Password ! ");
                }
            }
        });

        return view;
    }

    public void setOnUnlockWithMasterPasswordListener(OnUnlockWithMasterPassword onUnlockWithMasterPassword) {
        mOnUnlockWithMasterPassword = onUnlockWithMasterPassword;
    }

    public interface OnUnlockWithMasterPassword {
        void invalidate();
    }

}
