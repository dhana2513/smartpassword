package com.supertech.smartpassword.utils;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.supertech.smartpassword.activities.main.adapter.UserData;
import com.supertech.smartpassword.activities.main.adapter.UserImage;

import java.io.File;
import java.util.ArrayList;

public class DBUtil {
    @SuppressLint("StaticFieldLeak")
    private static DBUtil dbUtil;
    private SQLiteDatabase mDbUserData;
    private Context mContext;

    private DBUtil(Context context) {
        mContext = context;
        mDbUserData = new ClsDatabaseHelper(context).getWritableDatabase();
    }

    public static DBUtil getInstance(Context context) {
        if (dbUtil == null) {
            dbUtil = new DBUtil(context);
        }

        return dbUtil;
    }

    public void insert(byte[] userName, byte[] password, String description) {

        ContentValues values = new ContentValues();
        values.put("USERNAME", userName);
        values.put("PASSWORD", password);
        values.put("DESCRIPTION", description);

        mDbUserData.insert("TblUnamePswd", null, values);

    }

    void insertImage(String path) {
        ContentValues values = new ContentValues();
        values.put("PATH", path);
        mDbUserData.insert("TblImages", null, values);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("com.supertech.NewImageAdded"));
    }

    public void deleteImage(UserImage userImage) {
        mDbUserData.execSQL("delete from TblImages where ID = " + userImage.getId());
        File file = new File(userImage.getPath());
        file.delete();
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("com.supertech.NewImageAdded"));
    }

    public void putPattern(byte[] patten) {
        mDbUserData.delete("TblPattern","",null);
        ContentValues values = new ContentValues();
        values.put("Pattern", patten);
        mDbUserData.insert("TblPattern", null, values);
    }

    public byte[] getPattern() {
        @SuppressLint("Recycle")
        Cursor c = mDbUserData.rawQuery("Select * from TblPattern", null);
        if (c.moveToNext()) {
            return c.getBlob(0);
        }
        return null;
    }

    public ArrayList<UserData> getAllDatabaseData() {
        ArrayList<UserData> arylst_userdata = new ArrayList<>();

        try (Cursor c = mDbUserData.rawQuery("Select * from TblUnamePswd", null)) {

            if (c != null) {
                if (c.getCount() != 0) {
                    EncryptionDecryptionUtil decryptString = EncryptionDecryptionUtil.getInstance(mContext);
                    while (c.moveToNext()) {
                        arylst_userdata.add(new UserData(c.getInt(0), decryptString.decrypt(c.getBlob(1)), decryptString.decrypt(c.getBlob(2)), c.getString(3)));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arylst_userdata;
    }

    public ArrayList<UserImage> getAllImages() {
        ArrayList<UserImage> listBitmaps = new ArrayList<>();

        try (Cursor c = mDbUserData.rawQuery("Select * from TblImages", null)) {

            if (c != null) {
                if (c.getCount() != 0) {
                    while (c.moveToNext()) {
                        listBitmaps.add(new UserImage(c.getInt(0), c.getString(1)));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listBitmaps;
    }

    public void delete(int position) {
        try {
            mDbUserData.execSQL("delete from TblUnamePswd where ID = " + position);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(int id, byte[] username, byte[] pswd, String description) {
        try {
            ContentValues values = new ContentValues();
            values.put("USERNAME",username);
            values.put("PASSWORD",pswd);
            values.put("DESCRIPTION",description);
            mDbUserData.update("TblUnamePswd",values,"ID="+id,null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deletePattern() {
        mDbUserData.delete("TblPattern", "", null);
    }


    public class ClsDatabaseHelper extends SQLiteOpenHelper {
        private static final int DATABASE_VERSION = 1;
        private static final String DATABASE_NAME = "PASSWORD_DB";

        ClsDatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE TblUnamePswd(ID INTEGER PRIMARY KEY AUTOINCREMENT, USERNAME BLOB,PASSWORD BLOB,DESCRIPTION TEXT)");
            db.execSQL("CREATE TABLE TblImages(ID INTEGER PRIMARY KEY AUTOINCREMENT, PATH TEXT)");
            db.execSQL("CREATE TABLE TblPattern(Pattern BLOB)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    }

}
