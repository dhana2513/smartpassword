package com.supertech.smartpassword.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionDecryptionUtil {

    private static EncryptionDecryptionUtil instance;
    private SecretKey mSecretKey;

    public static EncryptionDecryptionUtil getInstance(Context context) {
        if (instance == null)
            instance = new EncryptionDecryptionUtil(context);
        return instance;
    }

    private EncryptionDecryptionUtil(Context context) {
        byte[] masterpassword = com.supertech.smartpassword.utils.SessionManagerUtil.getInstance(context).getMasterpassword();
        mSecretKey = new SecretKeySpec(masterpassword, "AES");
    }


    public byte[] encrypt(String message) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, mSecretKey);
            return cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public String decrypt(byte[] cipherText) {
        try {
            @SuppressLint("GetInstance")
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, mSecretKey);
            return new String(cipher.doFinal(cipherText), StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.getStackTrace();
            return null;
        }
    }

   public Bitmap decryptBitmap(String path) {
        try {
            @SuppressLint("GetInstance")
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, mSecretKey);
            File file = new File(path);
            int size = (int) file.length();
            byte[] fileBytes = new byte[size];
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(fileBytes, 0, fileBytes.length);
            buf.close();
            byte[] bytes = cipher.doFinal(fileBytes);
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        } catch (Exception e) {
            e.getStackTrace();
            return null;
        }
    }

    public static byte[] encryptMasterPassword(String message) {
        try {
            String masterpassword = "0123456789abcdef";
            SecretKey secretKey = new SecretKeySpec(masterpassword.getBytes(StandardCharsets.UTF_8), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static String decryptMasterPassword(byte[] cipherText) {
        try {
            String masterpassword = "0123456789abcdef";
            SecretKey secretKey = new SecretKeySpec(masterpassword.getBytes(StandardCharsets.UTF_8), "AES");
            @SuppressLint("GetInstance")
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(cipherText), StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.getStackTrace();
            return null;
        }
    }

    public void saveImage(Bitmap bitmap, String path) {
        new EncryptBitmapAsync(path).execute(bitmap);
    }

    @SuppressLint("StaticFieldLeak")
    private class EncryptBitmapAsync extends AsyncTask<Bitmap, Void, byte[]> {
        private String path;

        EncryptBitmapAsync(String path) {
            this.path = path;
        }

        @Override
        protected byte[] doInBackground(Bitmap... bitmaps) {
            try {
                Bitmap bitmap = bitmaps[0];
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                ByteArrayOutputStream blob = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, blob);
                byte[] bitmapdata = blob.toByteArray();
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, mSecretKey);
                if (!path.isEmpty()) {
                    File file = new File(path);
                    file.delete();
                }
                return cipher.doFinal(bitmapdata);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(byte[] bytes) {
            super.onPostExecute(bytes);
            try {
                path = path.replace(".jpg", ".txt");
                File file = new File(path);
                FileOutputStream outputStream = new FileOutputStream(file);
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }

            com.supertech.smartpassword.utils.DBUtil.getInstance(null).insertImage(path);

        }
    }

}
