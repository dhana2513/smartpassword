package com.supertech.smartpassword.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

public class SessionManagerUtil {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private static SessionManagerUtil mSessionManagerUtil;


    private static final String REG_LIB_PREF_NAME = "Session_password";

    public static SessionManagerUtil getInstance(Context context) {
        if (mSessionManagerUtil == null) {
            mSessionManagerUtil = new SessionManagerUtil(context);
        }
        return mSessionManagerUtil;
    }

    @SuppressLint("CommitPrefEdits")
    private SessionManagerUtil(Context context) {
        try {
            int PRIVATE_MODE = 0;
            pref = context.getSharedPreferences(REG_LIB_PREF_NAME, PRIVATE_MODE);
            editor = pref.edit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final String PATTERN_SET = "PATTERN_SET";
    private static final String LOCK_SMART_PASSWORD = "LOCK_SMART_PASSWORD";
    private static final String MASTER_PASSWORD = "MASTER_PASSWORD";


    public void setIsPattenSaved(boolean yesno) {
        editor.putBoolean(PATTERN_SET, yesno);
        editor.commit();
    }


    public boolean getIsPatternSaved() {
        return pref.getBoolean(PATTERN_SET, false);
    }

    public void setLock(boolean yesno) {
        editor.putBoolean(LOCK_SMART_PASSWORD, yesno);
        editor.commit();
    }

    public boolean getIsLocked() {
        return pref.getBoolean(LOCK_SMART_PASSWORD, false);
    }

    public void setMasterpassword(byte[] pass) {
        String password = Base64.encodeToString(pass, Base64.DEFAULT);
        editor.putString(MASTER_PASSWORD, password).commit();
    }

    public byte[] getMasterpassword() {
        String password = pref.getString(MASTER_PASSWORD, "");
        if(password.isEmpty()){
            return null;
        }
        return Base64.decode(password, Base64.DEFAULT);
    }
}
